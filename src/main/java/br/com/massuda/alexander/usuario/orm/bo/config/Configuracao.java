/**
 * 
 */
package br.com.massuda.alexander.usuario.orm.bo.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * @author Alex
 *
 */
@Import({br.com.massuda.alexander.usuario.dao.config.Configuracao.class})
@Configuration
@ComponentScan("br.com.massuda.alexander.usuario.orm.bo")
public class Configuracao {

//	@Bean
//	public BOUsuario getBOUsuarioImpl() {
//		return new BOUsuarioImpl();
//	}
}
