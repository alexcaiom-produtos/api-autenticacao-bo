package br.com.massuda.alexander.usuario.orm.bo;

import br.com.waiso.framework.abstratas.Classe;

public abstract class BOImpl<ChavePrimaria, T> extends Classe implements IBO<ChavePrimaria, T> {

	public String getNomeEntidade(){
		return CLASSE_NOME.substring(2);
	}
	
}
